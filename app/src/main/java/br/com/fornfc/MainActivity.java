package br.com.fornfc;


import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends Activity {

    NfcAdapter nfcAdapter;
    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getDateTimeInstance();
    ToggleButton tglReadWrite;
    TextView txtTagContent;
    TextView txtTimeContent;
    TextView txtLongContent;
    TextView txtLatContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        tglReadWrite = (ToggleButton)findViewById(R.id.tglReadWrite);
        txtTagContent = (TextView)findViewById(R.id.txtTagContent);
        txtTimeContent = (TextView)findViewById(R.id.txtTimeContent);
        txtLongContent = (TextView)findViewById(R.id.txtLongContent);
        txtLatContent = (TextView)findViewById(R.id.txtLatContent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        enableForegroundDispatchSystem();
    }

    @Override
    protected void onPause() {
        super.onPause();

        disableForegroundDispatchSystem();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            if(tag != null)
            {
                byte[] id = tag.getId();
                txtTagContent.setText(String.valueOf(toDec(id)));
                Date now = getTimeNow();
                String timestamp = String.valueOf(now.getTime());
                txtTimeContent.setText(TIME_FORMAT.format(now));
                txtLatContent.setText("0.0");
                txtLongContent.setText("0.0");

            }else{
                Toast.makeText(this, "No NDEF messages found!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private Date getTimeNow() {
        Date now = new Date();
        return now;
    }

    private long toDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private void enableForegroundDispatchSystem() {

        Intent intent = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    private void disableForegroundDispatchSystem() {
        nfcAdapter.disableForegroundDispatch(this);
    }

    public void tglReadWriteOnClick(View view){
        txtTagContent.setText("");
    }


}
